test-dev
========

Un stagiaire à créer le code contenu dans le fichier AppBundle/Controller/DefaultController.php
 

Celui permet de récupérer des urls via un flux RSS ou un appel à l’API NewsApi. Celles ci sont filtrées (si contient une image) et dé doublonnées. Enfin, il faut récupérer une image sur chacune de ces pages.  


Le lead dev n'est pas très satisfait du résultat, il va falloir améliorer le code.


1. Revoir complètement la conception du code (découper le code afin de pouvoir ajouter de nouveaux flux simplement) 
2. Que mettriez-vous en place afin d'améliorer les temps de réponses du script

    - Des appels AJAX en front pour charger les images au fur et à mesure

3. Comment aborderiez-vous le fait de rendre scalable le script (plusieurs milliers de sources et images)
    - Scinder la partie récupération des url avec la partie récupération des images et effectuer des traitements par lots.
    - Mettre en place une commande d'import des flux et des images et les stocker en BDD. Le front chargera les images stockées en base.
    - Mettre en place un système de parallélisme ? (pour lancer plusieurs commandes d'import des flux)



Lancement des tests :
vendor\bin\simple-phpunit
