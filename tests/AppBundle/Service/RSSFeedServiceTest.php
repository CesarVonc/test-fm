<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Service\RSSFeedService;
use AppBundle\Service\UtilService;
use PHPUnit\Framework\TestCase;

class RSSFeedServiceTest extends TestCase
{
    private $rssFeedService;
    private $utilService;

    const XML = '<?xml version="1.0" encoding="UTF-8"?>
	<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:media="http://search.yahoo.com/mrss/" xmlns:wp="http://wordpress.org/export/1.2/" xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/"  >
		<channel>
		<title>CommitStrip</title>
        <description>The blog relating the daily life of web agency developers</description>
        <link>http://www.commitstrip.com</link>
		<lastBuildDate>Tue, 14 Jan 2020 19:03:56 +0000</lastBuildDate>
					<item>
							<title><![CDATA[Other people’s code]]></title>
							<link><![CDATA[http://www.commitstrip.com/en/2020/01/14/other-peoples-code/]]></link>
							<pubDate>Tue, 14 Jan 2020 20:03:56 +0000</pubDate>
							<dc:creator>CommitStrip</dc:creator>
							<dc:identifier>20820</dc:identifier>
							<dc:modified>2020-01-14 20:03:56</dc:modified>
							<dc:created unix="1579032236">2020-01-14 20:03:56</dc:created>
							<guid isPermaLink="true"><![CDATA[http://www.commitstrip.com/en/2020/01/14/other-peoples-code/]]></guid><category>1</category>
							<description><![CDATA[]]></description><content:encoded><![CDATA[<img src="https://www.commitstrip.com/wp-content/uploads/2020/01/Strip-Paywall-650-finalenglish.jpg" alt="" width="650" height="607" class="alignnone size-full wp-image-20822" />]]></content:encoded><enclosure url="http://www.commitstrip.com/wp-content/uploads/2020/01/HeadlineImageTemplate-Récupéré-1.jpg"/><media:content url="http://www.commitstrip.com/wp-content/uploads/2020/01/HeadlineImageTemplate-Récupéré-1.jpg" height="492" width="940" type="image/jpeg"/>		
					</item>
					<item>
							<dc:creator>CommitStrip</dc:creator>
                    </item>
					<item>
							<link><![CDATA[http://a/]]></link>
                    </item>
					<item>
							<title>test sans image</title>
							<link><![CDATA[http://b/]]></link>
							<pubDate>Wed, 08 Jan 2020 18:02:27 +0000</pubDate>
							<guid isPermaLink="true"><![CDATA[http://www.commitstrip.com/en/2020/01/08/unintentional-framework/]]></guid><category>1</category>
							<description><![CDATA[]]></description>
							<content:encoded><![CDATA[]]></content:encoded><enclosure url="http://www.commitstrip.com/wp-content/uploads/2020/01/HeadlineImageTemplate-Récupéré.jpg"/><media:content url="http://www.commitstrip.com/wp-content/uploads/2020/01/HeadlineImageTemplate-Récupéré.jpg" height="492" width="940" type="image/jpeg"/>		
					</item>
					<item>
							<title><![CDATA[Unintentional framework]]></title>
							<link><![CDATA[http://www.commitstrip.com/en/2020/01/08/unintentional-framework/]]></link>
							<pubDate>Wed, 08 Jan 2020 18:02:27 +0000</pubDate>
							<dc:creator>CommitStrip</dc:creator>
							<dc:identifier>20814</dc:identifier>
							<dc:modified>2020-01-08 18:02:27</dc:modified>
							<dc:created unix="1578506547">2020-01-08 18:02:27</dc:created>
							<guid isPermaLink="true"><![CDATA[http://www.commitstrip.com/en/2020/01/08/unintentional-framework/]]></guid><category>1</category>
							<description><![CDATA[]]></description><content:encoded><![CDATA[<img src="https://www.commitstrip.com/wp-content/uploads/2020/01/Strip-Framework-malgré-soi-650-finalenglish.jpg" alt="" width="650" height="616" class="alignnone size-full wp-image-20816" />]]></content:encoded><enclosure url="http://www.commitstrip.com/wp-content/uploads/2020/01/HeadlineImageTemplate-Récupéré.jpg"/><media:content url="http://www.commitstrip.com/wp-content/uploads/2020/01/HeadlineImageTemplate-Récupéré.jpg" height="492" width="940" type="image/jpeg"/>		
					</item>
					<item>
							<title><![CDATA[Unintentional framework]]></title>
							<link><![CDATA[http://www.commitstrip.com/en/2020/01/08/unintentional-framework/]]></link>
							<pubDate>Wed, 08 Jan 2020 18:02:27 +0000</pubDate>
							<dc:creator>CommitStrip</dc:creator>
							<dc:identifier>20814</dc:identifier>
							<dc:modified>2020-01-08 18:02:27</dc:modified>
							<dc:created unix="1578506547">2020-01-08 18:02:27</dc:created>
							<guid isPermaLink="true"><![CDATA[http://www.commitstrip.com/en/2020/01/08/unintentional-framework/]]></guid><category>1</category>
							<description><![CDATA[]]></description><content:encoded><![CDATA[<img src="https://www.commitstrip.com/wp-content/uploads/2020/01/Strip-Framework-malgré-soi-650-finalenglish.jpg" alt="" width="650" height="616" class="alignnone size-full wp-image-20816" />]]></content:encoded><enclosure url="http://www.commitstrip.com/wp-content/uploads/2020/01/HeadlineImageTemplate-Récupéré.jpg"/><media:content url="http://www.commitstrip.com/wp-content/uploads/2020/01/HeadlineImageTemplate-Récupéré.jpg" height="492" width="940" type="image/jpeg"/>		
					</item></channel></rss><!-- end of xml string -->';

    public function setUp(): void
    {
        $this->utilService = $this->prophesize(UtilService::class);
        $this->rssFeedService = new RSSFeedService($this->utilService->reveal());
    }

    public function testGetUrlsOk(): void
    {
        $this->utilService->cUrlGet('http://test')->willReturn(self::XML);
        $urls = $this->rssFeedService->getUrls('http://test');

        $this->assertEquals([
            'http://www.commitstrip.com/en/2020/01/14/other-peoples-code/',
            'http://www.commitstrip.com/en/2020/01/08/unintentional-framework/',
        ], $urls);
    }

    public function testGetUrlsFluxVide(): void
    {
        $this->utilService->cUrlGet('http://test')->willReturn('');

        $this->expectException('UnexpectedValueException');
        $this->expectExceptionMessage('Flux RSS vide.');

        $this->rssFeedService->getUrls('http://test');
    }

    public function testGetUrlsFluxSansChannel(): void
    {
        $this->utilService->cUrlGet('http://test')->willReturn(
            '<?xml version="1.0" encoding="UTF-8"?><rss><test></test></rss>'
        );

        $this->expectException('UnexpectedValueException');
        $this->expectExceptionMessage('Flux RSS erreur : node channel inexistant.');

        $this->rssFeedService->getUrls('http://test');
    }

    public function testGetUrlsFluxSansItem(): void
    {
        $this->utilService->cUrlGet('http://test')->willReturn(
            '<?xml version="1.0" encoding="UTF-8"?><rss><channel><test></test></channel></rss>'
        );

        $this->expectException('UnexpectedValueException');
        $this->expectExceptionMessage('Flux RSS erreur : node channel->item inexistant.');

        $this->rssFeedService->getUrls('http://test');
    }

}
