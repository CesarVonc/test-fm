<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Service\AbstractApiService;
use AppBundle\Service\UtilService;
use PHPUnit\Framework\TestCase;

class AbstractApiServiceTest extends TestCase
{
    private $abstractApiService;
    private $utilService;

    public function setUp(): void
    {
        $this->utilService = $this->prophesize(UtilService::class);
        $this->abstractApiService = new class('url', 'query', $this->utilService->reveal()) extends AbstractApiService {};
    }

    public function testGetUrlsRenvoieTableau(): void
    {
        $urls = $this->abstractApiService->getUrls();
        $this->assertIsArray($urls);
    }

    public function testGetQueryImageOk(): void
    {
        $queryImage = $this->abstractApiService->getQueryImage();
        $this->assertEquals('query', $queryImage);
    }

}
