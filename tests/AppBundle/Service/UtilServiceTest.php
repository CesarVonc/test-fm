<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Service\UtilService;
use PHPUnit\Framework\TestCase;

class UtilServiceTest extends TestCase
{
    private $utilService;
    private $urlCommitstrip;

    public function setUp(): void
    {
        $this->utilService = new UtilService();
        $this->urlCommitstrip = __DIR__.'\..\..\fixtures\commitstrip.html';
    }

    public function testGetImagesFromUrlsOk(): void
    {
        $url = $this->urlCommitstrip;

        $images = [];
        $this->utilService->getImagesFromUrls([$url], '//img[contains(@class,"size-full")]/@src', $images);

        $this->assertEquals([
            'https://www.commitstrip.com/wp-content/uploads/2020/01/Strip-Paywall-650-finalenglish.jpg'
        ], $images);
    }

}
