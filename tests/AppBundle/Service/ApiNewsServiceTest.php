<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Service\ApiNewsService;
use AppBundle\Service\UtilService;
use PHPUnit\Framework\TestCase;

class ApiNewsServiceTest extends TestCase
{
    private $apiNewsService;
    private $utilService;

    public function setUp(): void
    {
        $this->utilService = $this->prophesize(UtilService::class);
        $this->apiNewsService = new ApiNewsService('url', 'query', $this->utilService->reveal());
    }

    public function testGetUrlsOk(): void
    {
        $this->utilService->getApiJson('url', false)->willReturn(
            [
                'status' => 'ok',
                'articles' => [
                    [
                        'urlToImage' => 'testUrlImage',
                        'url' => 'testUrl',
                    ],
                    [
                        'urlToImage' => 'testUrlImage',
                    ],
                    [
                        'url' => 'testUrl',
                    ],
                    [
                        'test' => 'test',
                    ],
                ]
            ]
        );

        $urls = $this->apiNewsService->getUrls();

        $this->assertEquals(['testUrl'], $urls);
    }

    public function testGetUrlsReponseVide(): void
    {
        $this->utilService->getApiJson('url', false)->willReturn([]);
        $this->expectException('UnexpectedValueException');
        $this->expectExceptionMessage('Réponse JSON invalide.');
        $this->apiNewsService->getUrls();
    }

    public function testGetUrlsReponseKo(): void
    {
        $this->utilService->getApiJson('url', false)->willReturn(['status' => 'ko']);
        $this->expectException('UnexpectedValueException');
        $this->expectExceptionMessage('Réponse JSON invalide.');
        $this->apiNewsService->getUrls();
    }

    public function testGetUrlsSansArticles(): void
    {
        $this->utilService->getApiJson('url', false)->willReturn(['status' => 'ok']);
        $this->expectException('UnexpectedValueException');
        $this->expectExceptionMessage('Réponse sans articles ou invalides.');
        $this->apiNewsService->getUrls();
    }

    public function testGetUrlsArticlesInvalides(): void
    {
        $this->utilService->getApiJson('url', false)->willReturn(['status' => 'ok', 'articles' => 'test']);
        $this->expectException('UnexpectedValueException');
        $this->expectExceptionMessage('Réponse sans articles ou invalides.');
        $this->apiNewsService->getUrls();
    }
}
