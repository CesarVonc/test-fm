<?php

namespace AppBundle\Controller;

use AppBundle\Service\RSSFeedService;
use AppBundle\Service\ApiNewsService;
use AppBundle\Service\UtilService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /** @var array */
    private $rssFeed;

    /**
     * DefaultController constructor.
     */
    public function __construct(array $rssFeed)
    {
        $this->rssFeed = $rssFeed;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(
        RSSFeedService $rssFeedService,
        ApiNewsService $apiNewsService,
        UtilService $utilService
    )
    {
        $images = [];

        foreach ($this->rssFeed as $rss) {
            try {
                $urls = $rssFeedService->getUrls($rss['url']);
                $utilService->getImagesFromUrls($urls,$rss['queryImage'], $images);
            } catch (\Exception $exception) {
                // TODO : Log
            }
        }

        try {
            $urls = $apiNewsService->getUrls();
            $utilService->getImagesFromUrls($urls, $apiNewsService->getQueryImage(), $images);
        } catch (\Exception $exception) {
            // TODO : Log
        }

        return $this->render('default/index.html.twig', ['images' => $images]);
    }

}
