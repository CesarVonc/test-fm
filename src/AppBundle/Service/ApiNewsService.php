<?php

namespace AppBundle\Service;

class ApiNewsService extends AbstractApiService
{
    /**
     * @throws \Exception
     */
    private function getArticles(): array
    {
        $data = $this->utilService->getApiJson($this->apiUrl, false);

        if (empty($data) || empty($data['status']) || $data['status'] !== 'ok') {
            throw new \UnexpectedValueException('Réponse JSON invalide.');
        }

        if (!isset($data['articles']) || !is_array($data['articles'])) {
            throw new \UnexpectedValueException('Réponse sans articles ou invalides.');
        }

        return $data['articles'];
    }

    /**
     * @throws \Exception
     */
    public function getUrls(): array
    {
        $urls = [];

        $articles = $this->getArticles();

        foreach ($articles as $article) {
            if (empty($article['urlToImage']) || empty($article['url'])) {
                continue;
            }

            $urls[] = $article['url'];
        }

        return $urls;
    }
}
