<?php

namespace AppBundle\Service;

class UtilService
{
    /**
     * @throws \Exception
     */
    public function cUrlGet(string $url, bool $useSSL = true): string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);

        if (!$useSSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

        $data = curl_exec($ch);

        if (false === $data) {
            $erreur = curl_error($ch);
            curl_close($ch);
            throw new \Exception($erreur);
        }

        curl_close($ch);
        return $data;
    }

    /**
     * @throws \Exception
     */
    public function getApiJson(string $url, bool $useSSL = true)
    {
        $data = $this->cUrlGet($url, $useSSL);
        if (!$data) return [];

        $json = json_decode($data, true);
        if (false === $json) return [];

        return $json;
    }

    private function getImageFromUrl(string $url, string $queryImage): string
    {
        libxml_use_internal_errors(false); // Suppression de l'erreur du DOCTYPE des pages

        $domDocument = new \DomDocument();

        try {
            $domDocument->loadHTMLFile($url);
        }
        catch (\Exception $exception) {
            // TODO : Log
        }

        libxml_use_internal_errors(true);

        $xpath = new \DomXpath($domDocument);
        $xq = $xpath->query($queryImage);

        if ($xq && !empty($xq[0]->value)) {
            return $xq[0]->value;
        }

        return '';
    }

    public function getImagesFromUrls(array $urls, string $queryImage, array &$images): array
    {
        foreach ($urls as $url) {
            $image = $this->getImageFromUrl($url, $queryImage);

            if ($image) {
                $images[] = $image;
            }
        }

        return $images;
    }
}
