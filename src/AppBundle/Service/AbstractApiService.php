<?php

namespace AppBundle\Service;

abstract class AbstractApiService
{
    /** @var UtilService  */
    protected $utilService;

    /** @var string */
    protected $apiUrl;

    /** @var string */
    protected $queryImage;

    public function __construct(string $apiUrl, string $queryImage, UtilService $utilService)
    {
        $this->utilService = $utilService;
        $this->apiUrl = $apiUrl;
        $this->queryImage = $queryImage;
    }

    /**
     * @throws \Exception
     */
    public function getUrls(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getQueryImage(): string
    {
        return $this->queryImage;
    }


}
