<?php

namespace AppBundle\Service;

class RSSFeedService
{
    /** @var UtilService  */
    private $utilService;

    public function __construct(UtilService $utilService)
    {
        $this->utilService = $utilService;
    }

    /**
     * @throws \UnexpectedValueException
     * @throws \Exception
     */
    public function getUrls(string $url): array
    {
        $xml = $this->getFeedXml($url);

        if (!isset($xml->channel)) {
            throw new \UnexpectedValueException('Flux RSS erreur : node channel inexistant.');
        }

        if (!isset($xml->channel->item)) {
            throw new \UnexpectedValueException('Flux RSS erreur : node channel->item inexistant.');
        }

        $urls = [];

        foreach ($xml->channel->item as $item) {

            if (!empty($item->link)) {

                $link = (string)$item->link;

                if ($this->contentHasImage($item)) {

                    $urls[] = $link;
                }
            }
        }

        return array_unique($urls);
    }

    /**
     * @throws \Exception
     */
    private function getFeedString(string $url)
    {
        $data = $this->utilService->cUrlGet($url);

        $endOfXml = strpos($data, '<!-- end of xml string -->');

        if (false !== $endOfXml) {
            $data = substr($data, 0, $endOfXml);
        }

        if (!$data) {
            throw new \UnexpectedValueException('Flux RSS vide.');
        }

        return $data;
    }

    /**
     * @throws \Exception
     */
    private function getFeedXml(string $url): \SimpleXMLElement
    {
        $data = $this->getFeedString($url);
        return new \SimpleXMLElement($data, LIBXML_NOCDATA);
    }

    private function contentHasImage(\SimpleXMLElement $node): bool
    {
        $imageNode = $node->children('content', true);

        if (!$imageNode) {
            return false;
        }

        $imageString = (string)$imageNode;
        preg_match('/<img[^>]+src="([^">]+)\.(jpg|png|gif)"/i', $imageString, $imageMatches);

        if (\count($imageMatches) === 3) {
            return true;
        }

        return false;
    }

}
